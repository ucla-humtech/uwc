<?php         
    // CODE to help figure out which section is displaying
       // This check the url and make sure the elements switch..
    $url = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
    $switchURL = 'wp';
    if ((strpos($url,'uwc') !== false) ||(strpos($url,'wc') !== false)) {

        $switchURL = 'uwc';
    } elseif (strpos($url,'esl') !== false) {
        $switchURL = 'esl';
    } else {
        $switchURL = 'wp';
    }
?>
<?php if ( is_front_page() ) { ?>
<div class="selection-title">
    <div class="content" >         
        <h2>Choose A Program</h2>
        <?php get_search_form(); ?> 
    </div>
</div>
<?php } else { ?>

<div class="section-nav">
    <div class="content" >
        
    <?php $home_loop = new WP_Query( array( 'pagename' => 'home') ); ?>
    <?php while ( $home_loop->have_posts() ) : $home_loop->the_post(); ?>
        <?php 
              // If set to Slider
					if(get_field('hero_type', 'option') == "single") { ?>
						<ul id="nav-slides">
							<?php if( have_rows('menu_items') ): ?>
							<?php while( have_rows('menu_items') ): the_row(); ?>
							<?php
								$slider_title = get_sub_field('title');
								$slider_description = get_sub_field('title_acronym');
								$slider_link = get_sub_field('link');
								$silder_image = get_sub_field('image');                                                                      
								$slider_icon = get_sub_field('icon');
								if( !empty($silder_image) ): 
									// vars
									$url = $silder_image['url'];
									$title = $silder_image['title'];
									// thumbnail
									$size = 'section-hero-thumb';
									$slide = $silder_image['sizes'][ $size ];
									$width = $silder_image['sizes'][ $size . '-width' ];
									$height = $silder_image['sizes'][ $size . '-height' ];
								endif;
                                                                      
                                if( !empty($slider_icon) ): 
									// vars
									$url = $slider_icon['url'];
									$title = $slider_icon['title'];
									// thumbnail
									$size = 'menu-icons';
									$icon = $slider_icon['sizes'][ $size ];
									$width = $slider_icon['sizes'][ $size . '-width' ];
									$height = $slider_icon['sizes'][ $size . '-height' ];
								endif;
							?>		
							<?php if( $slider_link ): ?>
							<a href="<?php echo $slider_link; ?>" class="hero-link">
							<?php endif; ?>
							<li class="<?php echo $slider_description; ?> <?php if($switchURL == $slider_description){ echo active; } ?>">
                                         
                            <div class="select-section <?php if($slider_description){ echo $slider_description; } ?>-btn <?php if($switchURL == $slider_description){ echo active; } ?>" style="background-image: url('<?php echo $slide; ?>');">
								<div class="content">
								</div>            
                            </div>
									<div class="slider-content">
										<?php if( $slider_title ): ?>
										<h3><?php echo $slider_title; ?></h3>
										<?php endif; ?>
                                        <?php if( $icon ): ?>
										  <div class="icon"><img src="<?php echo $icon; ?>" /></div>
										<?php endif; ?>
										<?php if( $slider_descriptions ): ?>
										  <p><?php echo $slider_description; ?></p>
										<?php endif; ?>
									</div>
							</li>
							<?php if( $slider_link ): ?>
							</a>
							<?php endif; ?>
							<?php endwhile; ?>
							<?php endif; ?>
						</ul>
				<?php }
        
        
					endwhile;	
              // Restore original Post Data
                wp_reset_postdata(); ?>
    </div>
</div>
<?php } ?>