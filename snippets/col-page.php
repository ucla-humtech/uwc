<div class="col page-col <?php the_sub_field('page_width'); ?>">
	<h3><?php the_sub_field('page_title'); ?></h3>
	<?php $page = get_sub_field('page');
		$page_query = new WP_Query( array( 'page_id' => $page ) );
		if ($page_query->have_posts()) : while ($page_query->have_posts()) : $page_query->the_post(); ?>
	<p>
		<?php $content = get_the_content();
			$limit = get_sub_field('word_limit');
			
		    $trimmed_content = wp_trim_words( $content, $limit, '...' );
		    echo $trimmed_content; 
		?>
	</p>
	<a class="btn" href="<?php the_permalink() ?>">Learn More<span class="hidden"> About Us</span></a>
	<?php endwhile; endif; ?>
	<?php wp_reset_postdata(); ?>
</div>