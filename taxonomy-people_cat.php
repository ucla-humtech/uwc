<?php get_header(); 

            // vars
            $queried_object = get_queried_object(); 
            $taxonomy = $queried_object->taxonomy;
            $term_id = $queried_object->term_id;
            $term_slug = $queried_object->slug;
            //echo $term_slug;
            
            $name_order = 'last_name';
            if ($term_slug == 'meet-the-staff-uwc'){
                $name_order = 'first_name';
            }else{
                $name_order = 'last_name';
            };
    		?>
			<div class="content main" id="main-content">
				<header>
					<h1><?php single_cat_title(); ?></h1>
					<?php $category_description = category_description();
					if ( ! empty( $category_description ) )
					echo apply_filters( 'category_archive_meta', '<p>' . $category_description . '</p>' );
					?>
					<?php if ( has_nav_menu( 'uwc-faculty-filter' ) ) {?> 
					<div class="filter">
					<?php // To make another filter, duplicate the div below ?>
						<div class="options button-group" data-filter-group="field">
							<h3>Field of Study</h3>
							<ul>
								<button data-filter="" data-text="All" class="option all is-checked">View All</button>
								<?php wp_nav_menu(array(
									'container' => false,
									'menu' => __( 'UWC Faculty Filter', 'bonestheme' ),
									'menu_class' => 'uwc-faculty-filter',
									'theme_location' => 'uwc-faculty-filter',
									'before' => '',
									'after' => '',
									'depth' => 1,
									'items_wrap' => '%3$s',
									'walker' => new Filter_Walker
								)); ?>
							</ul>
						</div>
					</div>
					<h2 class="filter-title">All</h2>
					<?php } ?> 
				</header>
				<div class="people-list">
					<ul <?php post_class('cf'); ?>>
					<?php $core_loop = new WP_Query( array( 'people_cat' => $term_slug, 'post_type' => 'people', 'posts_per_page' => -1, 'orderby' => 'meta_value', 'meta_key' => $name_order, 'order' => 'ASC')); ?>
					<?php while ( $core_loop->have_posts() ) : $core_loop->the_post(); ?>
						<li class="person-item<?php $areas = get_field('area_of_study'); if( $areas ): foreach( $areas as $area ): ?> <?php echo $area->slug; ?><?php endforeach; endif;?><?php $languages = get_field('language_of_study'); if( $languages ): foreach( $languages as $language ): ?> <?php echo $language->slug; ?><?php endforeach; endif;?>">
							<a href="<?php the_permalink() ?>">
								<?php // if there is a photo, use it
								if(get_field('photo')) {
									$image = get_field('photo');
									if( !empty($image) ): 
										// vars
										$url = $image['url'];
										$title = $image['title'];
										// thumbnail
										$size = 'people-thumb';
										$thumb = $image['sizes'][ $size ];
										$width = $image['sizes'][ $size . '-width' ];
										$height = $image['sizes'][ $size . '-height' ];
								endif; ?>
								<img src="<?php echo $thumb; ?>" alt="A photo of <?php the_title(); ?>" class="photo <?php if(get_field('corner_style', 'option') == "circle") { ?>circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
								<?php // otherwise use a silhouette 
								} else { ?>
								<img src="<?php echo get_template_directory_uri(); ?>/library/images/silhouette.jpg" alt="A photo of <?php the_title(); ?>" class="photo <?php if(get_field('corner_style', 'option') == "circle") { ?>circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
								<?php } ?>
								<dl>
									<dt class="name"><?php the_title(); ?></dt>
									<dd class="position"><?php the_field('position_title'); ?></dd>
									<?php if(get_field('interest')) { ?>
									   <dd class="interest"><?php the_field('interest'); ?></dd>
									<?php } ?>
                                    <?php if(get_field('affiliated_department')) { ?>
                                        <dd class="office">
                                            <strong>
                                                <?php if ($term_slug == 'meet-the-staff-uwc'){ ?>
                                                    Major:
                                                <?php }else{ ?>
                                                    Department: 
                                                <?php } ?>
                                            </strong>
                                            <?php the_field('affiliated_department'); ?>
                                        </dd>
                                    <?php } ?>
								</dl>
							</a>
						</li>
					<?php endwhile; ?>					
					</ul>
				</div>
			</div>
<?php get_footer(); ?>