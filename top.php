<?php
/*
	Basic Navigation and Hero 
*/
?>

<a href="#main-content" class="hidden skip">Skip to main content</a>
<div id="container">
	<header role="banner" class="top">
		<div class="content">
            <div class="icon-menu">
                <?php if(get_field('enable_donation', 'option') == "enable") { ?>
					<div class="give-back">
	                <?php if(get_field('link_type', 'option') == "internal") { ?>
	                	<a href="<?php the_field('donation_page', 'option'); ?>" class="give">
	                <?php }?>
	                <?php if(get_field('link_type', 'option') == "external") { ?>
	                	<a href="<?php the_field('donation_link', 'option'); ?>" class="give" target="_blank">
	                <?php }?>
	                    <img src="<?php echo get_template_directory_uri(); ?>/library/images/circle-heart-blk.png" />
	                    <p><?php the_field('button_text', 'option'); ?></p></a>
					</div>
            	<?php }?>
				 
				<?php if(get_field('appointment_url', 'option')) { ?>				
                <div class="icon">                         
                    <a href="<?php the_field('appointment_url', 'option'); ?>" class="icon-link">
                        <img src="<?php echo get_template_directory_uri(); ?>/library/images/bookit_top_icon_blk.png" alt="Book it! Link"  />
                        <?php if(get_field('button_label', 'option')) { ?>	
                            <p><?php the_field('button_label', 'option'); ?></p>
                        <?php } ?>
                    </a>
                </div>
				 <?php }?>
				 
				 <?php if(get_field('wp_url', 'option')) { ?>				
                <div class="icon">                         
                    <a href="<?php the_field('wp_url', 'option'); ?>" class="icon-link">
                        <img src="<?php echo get_template_directory_uri(); ?>/library/images/wp-logo-blk-33x33.png" alt="Book it! Link"  />
                        <?php if(get_field('wp_label', 'option')) { ?>	
                            <p><?php the_field('wp_label', 'option'); ?></p>
                        <?php } ?>
                    </a>
                </div>
				 <?php }?>
				 
            </div>
                                                
	        <a href="<?php echo home_url(); ?>" rel="nofollow">
				<img src="<?php echo get_template_directory_uri(); ?>/library/images/ucla-logo.svg" alt="UCLA" class="university-logo" /><span class="hidden">UCLA</span>
			    <div class="dept-logo"> 
					<img src="<?php echo get_template_directory_uri(); ?>/library/images/uwc-dept-logo.png" alt="<?php the_field('department_name', 'option'); ?>" class="dept-logo" />
	            </div>
            </a>               
		</div>
            
        <div class="nav-search">
            <div class="content">
                <nav aria-label="Main Navigation" class="desktop">
					<?php wp_nav_menu(array(
						'container' => false,
						'menu' => __( 'Main Menu', 'bonestheme' ),
						'menu_class' => 'main-nav',
						'theme_location' => 'main-nav',
						'before' => '',
						'after' => '',
						'depth' => 2,
					)); ?>
				</nav>
                <?php get_search_form(); ?>      
            </div>            
        </div>
		
	</header>
	<?php 
		// Don't do any of the below if homepage
		if ( is_front_page() ) { }
		// Breadcrumb everywhere else
		elseif ( is_single() || is_category( $category ) || is_archive() ) { 
			if ( function_exists('yoast_breadcrumb') ) {
				yoast_breadcrumb('<div class="breadcrumbs"><div class="content">','</div></div>');
			}
		}
		else {
			// Only show hero image on a page or post
			if ( has_post_thumbnail() && is_single() || has_post_thumbnail() && is_page() ) {
				$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'hero' );
				$url = $thumb['0']; ?>
	<div id="hero" style="background-image: url('<?=$url?>');">
		&nbsp;
	</div>
	<?php }
			// And show breadcrumb
            if ( function_exists('yoast_breadcrumb') ) {
                // To help make sure breadcrumbs don't show on internal home pages.
                if(!is_page_template( 'page-home.php' ) ) {
                yoast_breadcrumb('<div class="breadcrumbs"><div class="content">','</div></div>');
                }
            } 
        } 
	?>